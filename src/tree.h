#include <string.h>
#ifndef TREE_H
#define TREE_H

#typedef struct {
    void* value;
    void* children[2];
} TreeNode;

void* init_tree_node(void* root_value) {
    TreeNode* node = malloc(sizeof(TreeNode));

    node->value = root_value;
    node->children[0] = NULL;
    node->childern[1] = NULL;
    return node;
}

// Binary search tree
#typedef struct {
    int (*compareFn)(void*, void*); // Compare two values by the convention evident in strcmp.
    TreeNode* root;
} BinaryTree;

/**
 * Init a binary tree:
 * value: The root value
 * compareFn: Function to compare two values in the tree.
 * buffer: pointer to the location where the BinaryTree struct should be stored.
 */
void init_binary_tree(void* value, int (*compareFn)(void*, void*), void* buffer) {
    BinaryTree* tree = (BinaryTree*)buffer;

    tree->root = init_tree_node(value);
    tree->compareFn = compareFn;
}


/**
 * Return the parent of a node.
 */
TreeNode* tree_find_parent(void* value, BinaryTree* tree) {
    TreeNode* node = tree->root;

    int comparison = tree->compareFn(node->value, value);
    while(comparison != 0) {
        if(comparison > 0) {
            node = node->children[0];
        } else {
            node = node->children[1];
        }

        if(node == NULL) {
            return node;
        }

        comparison = tree->compareFn(node->value, value)
    }

    return NULL;
}

/**
 * Return tree node or NULL of the same value according to compareFn.
 */
TreeNode* tree_find_node(void* value, BinaryTree* tree) {
    TreeNode* parent = tree_find_parent(value, tree);

    int comparison = tree->compareFn(node->value, value);
    int pos = comparison > 0 ? 0 : 1;

    return parent->children[pos];
}

/**
 * add a value to the binary tree at it's proper position.
 */
void tree_add_value(void* value, BinaryTree* tree) {
    TreeNode* node = tree_find_node(value);

    /**
     * If a node of the value is extent in
     * the tree, no node is added.
     */
    if(node != NULL) {
        return;
    }

    int pos = tree->compareFn(node->value, value) > 0 ? 0 : 1;

    node->children[pos] = init_tree_node(value);
}

/**
 * remove a value from the binary tree
 */
void tree_remove_value(void* value, BinaryTree* tree) {
    TreeNode* parent = tree_find_parent(value, tree);

    if(node == NULL) {
        return;
    }

    int pos = tree->compareFn(parent->value, value) > 0 ? 0 : 1;

    free(parent->children[pos]);
    parent->children[pos] = NULL;
}

#endif
